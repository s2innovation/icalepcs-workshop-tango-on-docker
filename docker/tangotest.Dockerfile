FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install -y tango-test

ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /
RUN chmod +x /wait-for-it.sh

RUN { \
    echo '#!/bin/sh'; \
    echo '/usr/lib/tango/tango_admin --add-server TangoTest/$1 TangoTest sys/test/1'; \
    echo 'exec /usr/lib/tango/TangoTest "$@"'; \
    } | cat > /entrypoint.sh \
    && chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["01"]
