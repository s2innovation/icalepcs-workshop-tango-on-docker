FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install -y tango-db

ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /
RUN chmod +x /wait-for-it.sh

ENTRYPOINT ["/usr/lib/tango/DataBaseds"]
CMD ["2", "-ORBendPoint", "giop:tcp::10000"]
