FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install -y python-pytango

ENTRYPOINT ["/usr/bin/python"]
